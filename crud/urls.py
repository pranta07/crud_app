from django.urls import path
from .views import CrudListView, CrudDetailView, CrudCreateView, CrudUpdateView, CrudDeleteView

urlpatterns = [
    path('student/<int:pk>/delete/', CrudDeleteView.as_view(), name='delete'),
    path('student/<int:pk>/update/', CrudUpdateView.as_view(), name='update'),
    path('student/create/', CrudCreateView.as_view(), name='add_new'),
    path('student/<int:pk>/', CrudDetailView.as_view(), name='detail'),
    path('', CrudListView.as_view(), name='home'),
]
