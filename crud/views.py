from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView,CreateView,UpdateView,DeleteView
from .models import Post

# Create your views here.


class CrudListView(ListView):
    model = Post
    template_name = 'crud/home.html'


class CrudDetailView(DetailView):
    model = Post
    template_name = 'crud/detail.html'


class CrudCreateView(CreateView):
    model = Post
    template_name = 'crud/add_new.html'
    fields = ['name', 'student_id', 'department', 'intake']


class CrudUpdateView(UpdateView):
    model = Post
    template_name = 'crud/update.html'
    fields = ['student_id', 'department', 'intake']


class CrudDeleteView(DeleteView):
    model = Post
    template_name = 'crud/delete.html'
    success_url = reverse_lazy('home')

