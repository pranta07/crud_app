from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from .models import Post

# Create your tests here.
'''
class CrudTests(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username='testuser',
            email='test@email.com',
            password='secret'
        )
        self.post = Post.objects.create(
            name='pranta',
            student_id='16171103042',
            department='cse',
            intake='34',
        )
    def test_string_representation(self):
        post = Post(department='cse')
        self.assertEqual(str(post), post.department)


    def test_post_content(self):
        self.assertEqual(f'{self.post.student_id}', '16171103042')
        self.assertEqual(f'{self.post.department}', 'cse')
        self.assertEqual(f'{self.post.intake}', '34')

    def test_post_student_info(self):
        self.assertEqual(f'{self.post.name}', 'pranta'),
        self.assertEqual(f'{self.post.student_id}', '16171103042'),
        self.assertEqual(f'{self.post.department}', 'cse'),
        self.assertEqual(f'{self.post.intake}', '34')

    def test_post_list_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'pranta')
        self.assertTemplateUsed(response, 'crud/home.html')

    def test_post_detail_view(self):
        response = self.client.get('student/1/')
        no_response = self.client.get('student/100000/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'cse')
        self.assertTemplateUsed(response, 'crud/detail.html')
'''