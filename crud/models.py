from django.db import models
from django.urls import reverse

# Create your models here.


class Post(models.Model):
    name = models.CharField(max_length=200)
    student_id = models.IntegerField()
    department = models.CharField(max_length=20)
    intake = models.IntegerField()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('home')

